﻿using System;

namespace TaskManagement.Domain.Tasks.Exceptions
{
    public class DomainException: Exception
    {
        public DomainException(string message)
            : base(message)
        { }
    }
}
