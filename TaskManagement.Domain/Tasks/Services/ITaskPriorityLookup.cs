﻿using TaskManagement.Domain.Tasks.Aggregates;

namespace TaskManagement.Domain.Tasks.Services
{
    public interface ITaskPriorityLookup
    {
        TaskPriority FindPreviousTaskPriority(TaskPriority priority);
        TaskPriority FindNextTaskPriority(TaskPriority priority);
        TaskPriority FindLastTaskPriority();
    }
}
