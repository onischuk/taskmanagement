﻿using System;
using System.Collections.Generic;
using TaskManagement.Domain.Common;

namespace TaskManagement.Domain.Tasks.Aggregates
{
    public class TaskName: ValueObject<TaskName>
    {
        public string Value { get; private set; }

        private TaskName() { }

        public TaskName(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException(nameof(name), "Name cannot be empty");
            Value = name;
        }

        public static implicit operator TaskName(string name) => new TaskName(name);
        public static implicit operator string(TaskName name) => name.Value;

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Value;
        }
    }
}
