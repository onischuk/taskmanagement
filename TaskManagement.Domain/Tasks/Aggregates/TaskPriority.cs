﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using TaskManagement.Domain.Common;

[assembly: InternalsVisibleTo("TaskManagement.Tests")]

namespace TaskManagement.Domain.Tasks.Aggregates
{
    public class TaskPriority: ValueObject<TaskPriority>, IComparable<TaskPriority>
    {
        private const int HighestPriority = 1;
        private const int DefaultPriority = 0;
        internal bool IsHighest => HighestPriority == Value;
        internal bool IsDefault => DefaultPriority == Value;
        public int Value { get; private set; }

        private TaskPriority() { }
        
        internal TaskPriority(int priority)
        {
            if (priority < HighestPriority)
                throw new ArgumentException(nameof(priority), "Invalid priority value");         

            Value = priority;            
        }   
        
        public TaskPriority Decrease()
        {
            return new TaskPriority(Value + 1);
        }

        public TaskPriority Increase()
        {
            return new TaskPriority(Value - 1);
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Value;
        }

        public static TaskPriority Default()
        {
            var priority = new TaskPriority();
            priority.Value = DefaultPriority;
            return priority;
        }

        private static int Compare(TaskPriority first, TaskPriority second)
        {
            if (first == null && second == null)
                return 0;
            else if (first == null)
                return -1;
            else if (second == null)
                return 1;

            if (first.Value == second.Value)
            {
                if (first.Value == second.Value)
                    return 0;
                else if (first.Value < second.Value)
                    return -1;
                else
                    return 1;
            }
            else if (first.Value < second.Value)
                return -1;
            else
                return 1;
        }

        public int CompareTo(TaskPriority other)
        {
            return Compare(this, other);
        }

        public static implicit operator TaskPriority(int priority) => new TaskPriority(priority);

        public static implicit operator int(TaskPriority priority) => priority.Value;

        public static bool operator <(TaskPriority left, TaskPriority right)
        {
            return Compare(left, right) < 0;
        }

        public static bool operator >(TaskPriority left, TaskPriority right)
        {
            return Compare(left, right) > 0;
        }

    }
}
