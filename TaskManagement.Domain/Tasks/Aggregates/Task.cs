﻿using System;
using TaskManagement.Domain.Common;
using TaskManagement.Domain.Tasks.Exceptions;
using TaskManagement.Domain.Tasks.Services;

namespace TaskManagement.Domain.Tasks.Aggregates
{
    public class Task: IAggregateRoot
    {
        public Guid Id { get; private set; }

        public TaskPriority Priority { get; private set; }

        public TaskName Name { get; private set; }

        private Task() { }

        public Task(TaskName name, TaskPriority priority)
        {
            if(priority.IsDefault)
                throw new DomainException("Can not create task with default priority");

            Id = Guid.NewGuid();
            Name = name;
            Priority = priority;
        }

        public void RaisePriority(ITaskPriorityLookup taskPriorityLookup)
        {
            if (Priority.IsHighest)
                throw new DomainException("Can not raise priority fot task with highest priority");

            TaskPriority previousPriority = taskPriorityLookup.FindPreviousTaskPriority(Priority);
            TaskPriority previousForPreviousPriority = taskPriorityLookup.FindPreviousTaskPriority(previousPriority);

            if (previousPriority.IsDefault)
                throw new DomainException("Can not raise priority fot task with highest priority");

            TaskPriority newTaskPriority = previousPriority;

            if (previousPriority - previousForPreviousPriority > 1)
                newTaskPriority = previousForPreviousPriority.Decrease();

            Priority = new TaskPriority(newTaskPriority);
        }

        public void LowerPriority(ITaskPriorityLookup taskPriorityLookup)
        {
            int nextPriority = taskPriorityLookup.FindNextTaskPriority(Priority);
            if (nextPriority == Priority.Value)
                throw new DomainException("Can not lower priority for task with lowest priority");

            int previousPriority = taskPriorityLookup.FindPreviousTaskPriority(Priority);

            if (Priority.Value - previousPriority == 1)
                Priority = Priority.Decrease();
        }

        public void SetPriority(TaskPriority priority)
        { 
            Priority = priority;
        }
    }
}
