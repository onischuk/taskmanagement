﻿using System;
using TaskManagement.Domain.Common;

namespace TaskManagement.Domain.Tasks.Aggregates
{
    public interface ITaskRepository : IRepository<Task>
    {
        Task GetById(Guid id);
        System.Threading.Tasks.Task<Task> FindNextTaskByPriorityAsync(TaskPriority priority);
        System.Threading.Tasks.Task<Task> FindPreviousTaskByPriorityAsync(TaskPriority priority);
        System.Threading.Tasks.Task<Task> GetByIndexAsync(int index);
        void Add(Task task);
        void Remove(Task task);

    }
}