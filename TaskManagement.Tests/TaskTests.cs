using NUnit.Framework;
using TaskManagement.Domain.Tasks.Aggregates;
using TaskManagement.Domain.Tasks.Services;
using Moq;
using System.Collections.Generic;
using TaskManagement.Domain.Tasks.Exceptions;
using System;

namespace TaskManagement.Tests
{
    [TestFixture]
    public class TaskTests : BaseTest
    {  
        [Test]
        public void RaisePriority_ForMidLevelPriorityTask_ShouldUpdateTaskPriority()
        {
            var task = new Task(new TaskName("Test Task"), priority: 2);
            int expectedPriority = 1;
            var tasksPriorityList = new List<int> { 1, 2 }; 
            ITaskPriorityLookup priorityLookup = GetTaskPriorityLookup(tasksPriorityList);

            task.RaisePriority(priorityLookup);

            Assert.That(task.Priority.Value, Is.EqualTo(expectedPriority));
        }

        [Test]
        public void RaisePriority_ForTaskWIthHighestPriority_ShouldThrowDomainException()
        {
            var task = new Task(new TaskName("Test Task"), priority: 1);
            var tasksPriorityList = new List<int> { 1, 2 };
            ITaskPriorityLookup priorityLookup = GetTaskPriorityLookup(tasksPriorityList);

            Assert.Throws<DomainException>(() => task.RaisePriority(priorityLookup));
        }

        [Test]
        public void RaisePriority_ForTaskInListWithNotSequentialPriorities_ShouldSetPriorityOfPreviousTask()
        {
            var task = new Task(new TaskName("Test Task"), priority: 3);
            var tasksPriorityList = new List<int> { 1, 3 };
            int expectedPriority = 1;
            ITaskPriorityLookup priorityLookup = GetTaskPriorityLookup(tasksPriorityList);

            task.RaisePriority(priorityLookup);

            Assert.That(task.Priority.Value, Is.EqualTo(expectedPriority));
        }

        [Test]
        public void RaisePriority_ForTaskInListWithNotSequentialPriorities_ShouldSetPriorityGreaterThanPriorityOfThePreviousTask()
        {
            var task = new Task(new TaskName("Test Task"), priority: 4);
            var tasksPriorityList = new List<int> { 1, 3, 4 };
            int expectedPriority = 2;
            ITaskPriorityLookup priorityLookup = GetTaskPriorityLookup(tasksPriorityList);

            task.RaisePriority(priorityLookup);

            Assert.That(task.Priority.Value, Is.EqualTo(expectedPriority));
        }

        [Test]
        public void RaisePriority_ForFirstTaskInTheListButWithPriorityNotEquealsToOne_ShouldThrowDomainException()
        {
            var task = new Task(new TaskName("Test Task"), priority: 2);
            var tasksPriorityList = new List<int> { 2, 3 };
            ITaskPriorityLookup priorityLookup = GetTaskPriorityLookup(tasksPriorityList);

            Assert.Throws<DomainException>(() => task.RaisePriority(priorityLookup));
        }       

        [Test]
        public void LowerPriority_ForTaskWithNotLowestPriority_ShouldSetPriorityOfNextTask()
        {
            var task = new Task(new TaskName("Test Task"), priority: 2);
            var tasksPriorityList = new List<int> { 1, 2, 3 };
            int expectedPriority = 3;
            ITaskPriorityLookup priorityLookup = GetTaskPriorityLookup(tasksPriorityList);

            task.LowerPriority(priorityLookup);

            Assert.That(task.Priority.Value, Is.EqualTo(expectedPriority));
        }

        [Test]
        public void LowerPriority_ForTaskInListWithNotSequentialPreviousPriorities_ShouldNotUpdatePriority()
        {
            var task = new Task(new TaskName("Test Task"), priority: 3);
            var tasksPriorityList = new List<int> { 1, 3, 4 };
            int expectedPriority = 3;
            ITaskPriorityLookup priorityLookup = GetTaskPriorityLookup(tasksPriorityList);

            task.LowerPriority(priorityLookup);

            Assert.That(task.Priority.Value, Is.EqualTo(expectedPriority));
        }

        [Test]
        public void LowerPriority_ForTaskWithLowestPriority_ShouldThrowDomainException()
        {
            var task = new Task(new TaskName("Test Task"), priority: 2);
            var tasksPriorityList = new List<int> { 1, 2 };
            
            ITaskPriorityLookup priorityLookup = GetTaskPriorityLookup(tasksPriorityList);

            Assert.Throws<DomainException>(() => task.LowerPriority(priorityLookup));
        }

        [Test]
        public void SetPriority_WIthValidValue_ShouldUpdatePriority()
        {
            var task = new Task(new TaskName("Test Task"), priority: 1);
            var lookupMock = new Mock<ITaskPriorityLookup>();
            lookupMock.Setup(m => m.FindLastTaskPriority()).Returns(2);
            ITaskPriorityLookup priorityLookup = lookupMock.Object;
            int expectedPriority = 2;
            task.SetPriority(expectedPriority);

            Assert.That(task.Priority.Value, Is.EqualTo(expectedPriority));
        }        

        [Test]
        public void SetPriority_WithDefaultValue_ShouldThrowException()
        {
            var task = new Task(new TaskName("Test Task"), priority: 1);
            var lookupMock = new Mock<ITaskPriorityLookup>();
            lookupMock.Setup(m => m.FindLastTaskPriority()).Returns(1);
            ITaskPriorityLookup priorityLookup = lookupMock.Object;

            Assert.Throws<ArgumentException>(() => task.SetPriority(0));
        }

        [Test]
        public void SetPriority_WithNegativeValue_ShouldThrowException()
        {
            var task = new Task(new TaskName("Test Task"), priority: 1);
            var lookupMock = new Mock<ITaskPriorityLookup>();
            lookupMock.Setup(m => m.FindLastTaskPriority()).Returns(1);
            ITaskPriorityLookup priorityLookup = lookupMock.Object;

            Assert.Throws<ArgumentException>(() => task.SetPriority(-1));
        }        
    }
}