﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using TaskManagement.Application;
using TaskManagement.Application.Services;
using TaskManagement.Domain.Tasks.Aggregates;

namespace TaskManagement.Tests
{
    [TestFixture]
    public class TasksManagementTests: BaseTest
    {
        [Test]
        public void RaiseTaskPriority_ForTaskInListWithSequentialPriorities_ShouldLowerPriorityOfPreviousTask()
        {
            var tasksPriorityList = new List<int> { 1, 2 };
            (List<Task> taskList, TaskManager taskManager) = GetTaskManagerWithTasks(tasksPriorityList);
            Task firstTask = taskList[0];
            Task secondTask = taskList[1];
            int expectedPriorityForFirstTask = 2;
            int expectedPriorityForSecondTask = 1;            

            taskManager.RaiseTaskPriority(secondTask.Id).Wait();

            Assert.That(secondTask.Priority.Value, Is.EqualTo(expectedPriorityForSecondTask));
            Assert.That(firstTask.Priority.Value, Is.EqualTo(expectedPriorityForFirstTask));
        }


        [Test]
        public void RaiseTaskPriority_ForTaskInListWithNotSequentialPriorityOfPreviosTask_ShouldNotLowerPriorityOfPreviousTask()
        {
            var tasksPriorityList = new List<int> { 1, 3, 4 };
            (List<Task> taskList, TaskManager taskManager) = GetTaskManagerWithTasks(tasksPriorityList);
            Task firstTask = taskList[1];
            Task secondTask = taskList[2];
            int expectedPriorityForFirstTask = 3;
            int expectedPriorityForSecondTask = 2;

            taskManager.RaiseTaskPriority(secondTask.Id).Wait();

            Assert.That(secondTask.Priority.Value, Is.EqualTo(expectedPriorityForSecondTask));
            Assert.That(firstTask.Priority.Value, Is.EqualTo(expectedPriorityForFirstTask));
        }

        [Test]
        public void RaiseTaskPriority_ForTaskInListWithNotSequentialPriorityOfCurrentTask_ShouldLowerPriorityOfPreviousTaskByOne()
        {
            var tasksPriorityList = new List<int> { 1, 2, 4 };
            (List<Task> taskList, TaskManager taskManager) = GetTaskManagerWithTasks(tasksPriorityList);
            Task firstTask = taskList[1];
            Task secondTask = taskList[2];
            int expectedPriorityForFirstTask = 3;
            int expectedPriorityForSecondTask = 2;

            taskManager.RaiseTaskPriority(secondTask.Id).Wait();

            Assert.That(secondTask.Priority.Value, Is.EqualTo(expectedPriorityForSecondTask));
            Assert.That(firstTask.Priority.Value, Is.EqualTo(expectedPriorityForFirstTask));
        }

        [Test]
        public void LowerTaskPriority_ForTaskInListWithSequentialPriorities_ShouldRaisePriorityOfNextTask()
        {
            var tasksPriorityList = new List<int> { 1, 2 };
            (List<Task> taskList, TaskManager taskManager) = GetTaskManagerWithTasks(tasksPriorityList);
            Task firstTask = taskList[0];
            Task secondTask = taskList[1];
            int expectedPriorityForFirstTask = 2;
            int expectedPriorityForSecondTask = 1;

            taskManager.LowerTaskPriority(firstTask.Id).Wait();

            Assert.That(secondTask.Priority.Value, Is.EqualTo(expectedPriorityForSecondTask));
            Assert.That(firstTask.Priority.Value, Is.EqualTo(expectedPriorityForFirstTask));
        }

        [Test]
        public void LowerTaskPriority_ForTaskInListWithNotSequentialPrioritiesForNextTasks_ShouldRaisePriorityOfNextTask()
        {
            var tasksPriorityList = new List<int> { 1, 2, 4 };
            (List<Task> taskList, TaskManager taskManager) = GetTaskManagerWithTasks(tasksPriorityList);
            Task firstTask = taskList[1];
            Task secondTask = taskList[2];
            int expectedPriorityForFirstTask = 3;
            int expectedPriorityForSecondTask = 2;

            taskManager.LowerTaskPriority(firstTask.Id).Wait();

            Assert.That(secondTask.Priority.Value, Is.EqualTo(expectedPriorityForSecondTask));
            Assert.That(firstTask.Priority.Value, Is.EqualTo(expectedPriorityForFirstTask));
        }

        [Test]
        public void LowerTaskPriority_ForTaskInListWithNotSequentialPrioritiesForPreviousTasks_ShouldNotChangePriorityOfCurrentTask()
        {
            var tasksPriorityList = new List<int> { 1, 3, 4 };
            (List<Task> taskList, TaskManager taskManager) = GetTaskManagerWithTasks(tasksPriorityList);
            Task firstTask = taskList[1];
            Task secondTask = taskList[2];
            int expectedPriorityForFirstTask = 3;
            int expectedPriorityForSecondTask = 2;

            taskManager.LowerTaskPriority(firstTask.Id).Wait();

            Assert.That(secondTask.Priority.Value, Is.EqualTo(expectedPriorityForSecondTask));
            Assert.That(firstTask.Priority.Value, Is.EqualTo(expectedPriorityForFirstTask));
        }

        [Test]
        public void SetTaskPriority_NotLowerThanFotTaskWithLowestPriority_ShouldSwitchPriorityForCurrentAndReplacedTask()
        {
            var tasksPriorityList = new List<int> { 1, 2 };
            (List<Task> taskList, TaskManager taskManager) = GetTaskManagerWithTasks(tasksPriorityList);
            Task firstTask = taskList[0];
            Task secondTask = taskList[1];
            int expectedPriorityForFirstTask = 2;
            int expectedPriorityForSecondTask = 1;

            taskManager.SetTaskPriority(secondTask.Id, 1).Wait();

            Assert.That(secondTask.Priority.Value, Is.EqualTo(expectedPriorityForSecondTask));
            Assert.That(firstTask.Priority.Value, Is.EqualTo(expectedPriorityForFirstTask));
        }


        [Test]
        public void SetTaskPriority_LowerThanFotTaskWithLowestPriority_ShouldThrowException()
        {
            var tasksPriorityList = new List<int> { 1, 2 };
            (List<Task> taskList, TaskManager taskManager) = GetTaskManagerWithTasks(tasksPriorityList);  
            Task secondTask = taskList[1];
            Assert.ThrowsAsync<ArgumentException>(async () => await taskManager.SetTaskPriority(secondTask.Id, position: 3));
        }


    }
}
