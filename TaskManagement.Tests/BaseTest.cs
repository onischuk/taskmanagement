﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using TaskManagement.Application;
using TaskManagement.Application.Services;
using TaskManagement.Domain.Common;
using TaskManagement.Domain.Tasks.Aggregates;
using TaskManagement.Domain.Tasks.Services;
using SystemTask = System.Threading.Tasks.Task;

namespace TaskManagement.Tests
{
    public class BaseTest
    {
        protected ITaskPriorityLookup GetTaskPriorityLookup(IEnumerable<int> tasksPriorityList)
        {
            var lookupMock = new Mock<ITaskPriorityLookup>();
            
            lookupMock.Setup(m => m.FindPreviousTaskPriority(It.IsAny<TaskPriority>())).Returns<TaskPriority>(priority =>
            {
                int foundPriority = tasksPriorityList.OrderByDescending(p => p).FirstOrDefault(p => p < priority);
                if (foundPriority == default)
                    return TaskPriority.Default();
                return foundPriority;
            });

            lookupMock.Setup(m => m.FindNextTaskPriority(It.IsAny<TaskPriority>())).Returns<TaskPriority>(priority =>
            {
                int foundPriority = tasksPriorityList.FirstOrDefault(p => p > priority);

                return foundPriority == default ? priority : (TaskPriority)foundPriority;
            });

            lookupMock.Setup(m => m.FindLastTaskPriority()).Returns(tasksPriorityList.OrderByDescending(p => p).FirstOrDefault());

            return lookupMock.Object;
        }

        private List<Task> CreateTasksListFromPriorities(IEnumerable<int> tasksPriorityList)
        {
            return tasksPriorityList.Select((priority, index) => new Task($"Test task {index + 1}", priority)).ToList();
        }

        protected ITaskRepository GetTaskRepository(IEnumerable<Task> tasks)
        {
            var mockRepository = new Mock<ITaskRepository>();

            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.SaveChangesAsync(It.IsAny<CancellationToken>())).Returns(SystemTask.FromResult(1));
            mockRepository.Setup(m => m.UnitOfWork).Returns(mockUnitOfWork.Object);

            mockRepository.Setup(m => m.GetById(It.IsAny<Guid>())).Returns<Guid>(taskId => tasks.FirstOrDefault(t => t.Id == taskId));
            mockRepository.Setup(m => m.FindPreviousTaskByPriorityAsync(It.IsAny<TaskPriority>())).Returns<TaskPriority>(priority => SystemTask.FromResult(tasks.OrderByDescending(task => task.Priority.Value).FirstOrDefault(task => task.Priority.Value < priority.Value)));
            mockRepository.Setup(m => m.FindNextTaskByPriorityAsync(It.IsAny<TaskPriority>())).Returns<TaskPriority>(priority => SystemTask.FromResult(tasks.FirstOrDefault(task => task.Priority.Value > priority)));
            mockRepository.Setup(m => m.GetByIndexAsync(It.IsAny<int>())).Returns<int>(index =>
            {
                var tasksArray = tasks.ToArray();
                Task result = null;
                if (tasksArray.Length - 1 >= index)
                {
                    result = tasksArray[0];
                }
                return SystemTask.FromResult(result);

            });



            return mockRepository.Object;
        }

        protected (List<Task>, TaskManager) GetTaskManagerWithTasks(IEnumerable<int> tasksPriorityList)
        {
            ITaskPriorityLookup priorityLookup = GetTaskPriorityLookup(tasksPriorityList);
            List<Task> taskList = CreateTasksListFromPriorities(tasksPriorityList);
            ITaskRepository taskRepository = GetTaskRepository(taskList);
            return (taskList, new TaskManager(priorityLookup, taskRepository));
        }
    }
}
