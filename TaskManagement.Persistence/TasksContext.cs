﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TaskManagement.Domain.Common;
using TaskManagement.Domain.Tasks.Aggregates;

namespace TaskManagement.Persistence
{
    public class TasksContext : DbContext, IUnitOfWork
    {
        public TasksContext(DbContextOptions<TasksContext> options): base(options)
        {

        }

        public DbSet<Task> Tasks { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new TasksEntityTypeConfiguration());
        }
    }


    public class TasksEntityTypeConfiguration : IEntityTypeConfiguration<Task>
    {
        public void Configure(EntityTypeBuilder<Task> builder)
        {
            builder.HasKey(x => x.Id);
            builder.OwnsOne(x => x.Priority);
            builder.OwnsOne(x => x.Name);            
        }
    }

}
