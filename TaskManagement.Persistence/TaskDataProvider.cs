﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManagement.Application.DataProviders;
using TaskManagement.Application.Models;

namespace TaskManagement.Persistence
{
    public class TaskDataProvider : ITaskDataProvider
    {
        private readonly TasksContext context;

        public TaskDataProvider(TasksContext tasksContext)
        {
            context = tasksContext;
        }

        public Task<TaskModel> GetTaskAsync(Guid taskId)
        {
            return context.Tasks.AsNoTracking().Select(Task => new TaskModel
            {
                Id = Task.ToString(),
                TaskName = Task.Name
            }).FirstOrDefaultAsync();
        }

        public Task<List<TaskModel>> GetTasksAsync(int pageNumber, int pageSize)
        {            
            return context.Tasks.OrderBy(task => task.Priority.Value).Skip((pageNumber - 1) * pageSize).Take(pageSize).AsNoTracking().Select(Task => new TaskModel
            {
                Id = Task.Id.ToString(),
                TaskName = Task.Name
            }).ToListAsync();
        }
    }
}
