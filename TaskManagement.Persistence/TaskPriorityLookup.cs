﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using TaskManagement.Domain.Tasks.Aggregates;
using TaskManagement.Domain.Tasks.Services;

namespace TaskManagement.Persistence
{
    public class TaskPriorityLookup : ITaskPriorityLookup
    {
        private readonly TasksContext context;

        public TaskPriorityLookup(TasksContext context)
        {
            this.context = context;
        }

        public TaskPriority FindLastTaskPriority()
        {
            TaskPriority lastPriority = context.Tasks.AsNoTracking().OrderByDescending(task => task.Priority.Value).Select(task => task.Priority).FirstOrDefault();

            return lastPriority ?? TaskPriority.Default();
        }

        public TaskPriority FindNextTaskPriority(TaskPriority priority)
        {
            TaskPriority foundPriority = context.Tasks.AsNoTracking().OrderBy(task => task.Priority.Value).Select(task => task.Priority).FirstOrDefault(p => p.Value > priority.Value);
            return foundPriority ?? priority;
        }

        public TaskPriority FindPreviousTaskPriority(TaskPriority priority)
        {
            TaskPriority foundPriority  = context.Tasks.AsNoTracking().OrderByDescending(task => task.Priority.Value).Select(task => task.Priority).FirstOrDefault(p => p.Value < priority.Value);
            return foundPriority ?? TaskPriority.Default();
        }
    }
}
