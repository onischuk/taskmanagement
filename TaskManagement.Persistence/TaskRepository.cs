﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using TaskManagement.Domain.Common;
using TaskManagement.Domain.Tasks.Aggregates;

namespace TaskManagement.Persistence
{
    public class TaskRepository : ITaskRepository
    {
        private readonly TasksContext context;

        public TaskRepository(TasksContext context)
        {
            this.context = context;
        }

        public IUnitOfWork UnitOfWork => context;

        public void Add(Task task)
        {
            context.Tasks.Add(task);
        }

        public System.Threading.Tasks.Task<Task> FindNextTaskByPriorityAsync(TaskPriority priority)
        {
            return context.Tasks.OrderBy(task => task.Priority.Value).FirstOrDefaultAsync(task => task.Priority.Value > priority.Value);
        }

        public System.Threading.Tasks.Task<Task> FindPreviousTaskByPriorityAsync(TaskPriority priority)
        {
            return context.Tasks.OrderByDescending(task => task.Priority.Value).FirstOrDefaultAsync(task => task.Priority.Value < priority.Value);
        }

        public Task GetById(Guid id)
        {
           return context.Tasks.Find(id);
        }

        public System.Threading.Tasks.Task<Task> GetByIndexAsync(int index)
        {           
            return context.Tasks.OrderBy(task => task.Priority.Value).Skip(index).Take(1).FirstOrDefaultAsync();
        }

        public void Remove(Task task)
        {
            context.Tasks.Remove(task);
        }
    }
}
