﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TaskManagement.Application.DataProviders;
using TaskManagement.Application.Services;
using TaskManagement.Web.Models;

namespace TaskManagement.Web.Controllers
{
    [ApiController]
    [Route("api/tasks")]
    public class TasksController : ControllerBase
    {

        [HttpGet("getTask")]
        public async Task<ActionResult> GetTask(Guid taskId, [FromServices] ITaskDataProvider taskDataProvider)
        {
            return Ok(await taskDataProvider.GetTaskAsync(taskId));
        }

        [HttpGet("getTasks")]
        public async Task<ActionResult> GetTasks([FromServices] ITaskDataProvider taskDataProvider, int pageNumber = 1, int pageSize = 10)
        {
            return Ok(await taskDataProvider.GetTasksAsync(pageNumber, pageSize));
        }


        [HttpPost("CreateTask")]
        public async Task<ActionResult> CreateTask(NewTaskRequestModel model, [FromServices] ITaskManager taskManager)
        {
            Guid taskId = await taskManager.CreateTask(model.TaskName);
            return CreatedAtAction(nameof(GetTask), new { taskId });
        }


        [HttpDelete("RemoveTask")]
        public async Task<ActionResult> RemoveTask(TaskIdModel model, [FromServices] ITaskManager taskManager)
        {
            await taskManager.RemoveTask(model.Id);
            return Ok();
        }

        [HttpPut("setTaskPriority")]
        public async Task<ActionResult> SetPriority(SetTaskPriorityModel model, [FromServices] ITaskManager taskManager)
        {
            await taskManager.SetTaskPriority(model.Id, model.Position);
            return Ok();
        }

        [HttpPut("raiseTaskPriority")]
        public async Task<ActionResult> RaisePriority(TaskIdModel model, [FromServices] ITaskManager taskManager)
        {
            await taskManager.RaiseTaskPriority(model.Id);
            return Ok();
        }

        [HttpPut("lowerTaskPriority")]
        public async Task<ActionResult> LowerPriority(TaskIdModel model, [FromServices] ITaskManager taskManager)
        {
            await taskManager.LowerTaskPriority(model.Id);
            return Ok();
        }


    }
}