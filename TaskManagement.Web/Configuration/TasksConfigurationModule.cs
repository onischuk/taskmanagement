﻿using Autofac;
using Autofac.Core.Registration;
using TaskManagement.Application.DataProviders;
using TaskManagement.Application.Services;
using TaskManagement.Domain.Tasks.Aggregates;
using TaskManagement.Domain.Tasks.Services;
using TaskManagement.Persistence;

namespace TaskManagement.Web.Configuration
{
    public class TasksConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TaskPriorityLookup>().As<ITaskPriorityLookup>().InstancePerLifetimeScope();

            builder.RegisterType<TaskDataProvider>().As<ITaskDataProvider>().InstancePerLifetimeScope();
            builder.RegisterType<TaskManager>().As<ITaskManager>().InstancePerLifetimeScope();
            builder.RegisterType<TaskRepository>().As<ITaskRepository>().InstancePerLifetimeScope();
        }
    }
}
