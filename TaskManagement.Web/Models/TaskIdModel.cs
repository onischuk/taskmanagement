﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManagement.Web.Models
{
    public class TaskIdModel
    {
        public Guid Id { get; set; }
    }
}
