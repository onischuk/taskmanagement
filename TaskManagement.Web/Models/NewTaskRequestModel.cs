﻿namespace TaskManagement.Web.Models
{
    public class NewTaskRequestModel
    {
        public string TaskName { get; set; }
    }
}
