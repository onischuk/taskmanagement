﻿using System.ComponentModel.DataAnnotations;

namespace TaskManagement.Web.Models
{
    public class SetTaskPriorityModel : TaskIdModel
    {
        [Range(1, int.MaxValue)]
        public int Position { get; set; }
    }
}
