﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using TaskManagement.Persistence;
using Microsoft.Extensions.DependencyInjection;

namespace TaskManagement.Web.Extensions
{
    public static class DatabaseServiceCollectionExtensions
    {
        public static void EnsureDatabaseCreated(this IApplicationBuilder app)
        {
            var context = app.ApplicationServices.GetService<TasksContext>();

            if (!context.Database.EnsureCreated())
                context.Database.Migrate();
        }
    }
}
