﻿namespace TaskManagement.Application.Models
{
    public class TaskModel
    {
        public string Id { get; set; }
        public string TaskName { get; set; }
    }
}
