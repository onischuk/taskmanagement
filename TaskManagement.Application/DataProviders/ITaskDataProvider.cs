﻿using System;
using System.Collections.Generic;
using TaskManagement.Application.Models;

namespace TaskManagement.Application.DataProviders
{
    public interface ITaskDataProvider
    {
        System.Threading.Tasks.Task<TaskModel> GetTaskAsync(Guid taskId);
        System.Threading.Tasks.Task<List<TaskModel>> GetTasksAsync(int pageNumber, int pageSize);

    }
}
