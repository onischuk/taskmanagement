﻿using System;
using System.Threading.Tasks;

namespace TaskManagement.Application.Services
{
    public interface ITaskManager
    {
        Task<Guid> CreateTask(string name);
        Task RemoveTask(Guid id);
        Task RaiseTaskPriority(Guid taskId);
        Task LowerTaskPriority(Guid taskId);
        Task SetTaskPriority(Guid taskId, int position);
    }
}