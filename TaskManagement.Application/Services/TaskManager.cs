﻿using System;
using TaskManagement.Domain.Tasks.Aggregates;
using TaskManagement.Domain.Tasks.Exceptions;
using TaskManagement.Domain.Tasks.Services;
using SystemTask = System.Threading.Tasks.Task;

namespace TaskManagement.Application.Services
{
    public class TaskManager : ITaskManager
    {
        private readonly ITaskPriorityLookup taskPriorityLookup;
        private readonly ITaskRepository taskRepository;

        public TaskManager(ITaskPriorityLookup taskPriorityLookup, ITaskRepository taskRepository)
        {
            this.taskPriorityLookup = taskPriorityLookup;
            this.taskRepository = taskRepository;
        }

        public async System.Threading.Tasks.Task<Guid> CreateTask(string name)
        {
            TaskPriority lowestPriority = taskPriorityLookup.FindLastTaskPriority();
            var newTask = new Task(name, lowestPriority.Decrease());

            taskRepository.Add(newTask);
            await taskRepository.UnitOfWork.SaveChangesAsync();
            return newTask.Id;

        }

        public async SystemTask RemoveTask(Guid taskId)
        {
            Task removedTask = taskRepository.GetById(taskId);
            taskRepository.Remove(removedTask);
            await taskRepository.UnitOfWork.SaveChangesAsync();
        }

        public async SystemTask RaiseTaskPriority(Guid taskId)
        {
            Task currentTask = taskRepository.GetById(taskId);
            Task previousTask = await taskRepository.FindPreviousTaskByPriorityAsync(currentTask.Priority);          

            currentTask.RaisePriority(taskPriorityLookup);

            if (previousTask.Priority == currentTask.Priority)
            {
                previousTask.SetPriority(previousTask.Priority.Decrease());
            }

            await taskRepository.UnitOfWork.SaveChangesAsync();
        }

        public async SystemTask LowerTaskPriority(Guid taskId)
        {
            Task currentTask = taskRepository.GetById(taskId);
            Task nextTask = await taskRepository.FindNextTaskByPriorityAsync(currentTask.Priority);

            currentTask.LowerPriority(taskPriorityLookup);

            nextTask.SetPriority(currentTask.Priority.Increase());

            await taskRepository.UnitOfWork.SaveChangesAsync();

        }

        public async SystemTask SetTaskPriority(Guid taskId, int position)
        {
            int index = position - 1;
            Task replacedTask = await taskRepository.GetByIndexAsync(index);
            if (replacedTask is null)
                throw new ArgumentException("Can not set priority outside of existing tasks priorities list");
            int replacedTaskPriority = replacedTask.Priority;

            Task targetTask = taskRepository.GetById(taskId);
            int targetTaskPriority = targetTask.Priority;
            targetTask.SetPriority(replacedTaskPriority);
            replacedTask.SetPriority(targetTaskPriority);

            await taskRepository.UnitOfWork.SaveChangesAsync();
        }
    }
}
